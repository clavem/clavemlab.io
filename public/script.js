// Password Based Key Derivation with Web Cryptography API
//
// Copyright (c) 2015 Info Tech, Inc.
// Provided under the MIT license.
// See LICENSE file for details.

var host = "clavem.gitlab.io"
if (window.location.host == host && window.location.protocol != "https:")
{
    window.location.protocol = "https:";
}

function displayError(msg) {
    alert(msg);
    console.error(msg);
}

document.addEventListener("DOMContentLoaded", function()
{
    "use strict";

// Check that web crypto is even available
if (window.location.protocol == "https:" && (!window.crypto || !window.crypto.subtle))
{
    displayError("Your browser does not support the Web Cryptography API! This page will not work.");
    return;
}

// Check that encoding API is also available
if (!window.TextEncoder || !window.TextDecoder)
{
    displayError("Your browser does not support the Encoding API! This page will not work.");
    return;
}

// #################################################################################
// Visibility
// #################################################################################

var visibility = false;
var element = document.getElementsByClassName("visibility-btn");
for(var i = 0; i < element.length; i++)
{
    element[i].addEventListener("click", changeVisibility);
}

function changeVisibility()
{
    var aes_key_fields = document.getElementsByClassName("aes-key");
    var visibility_fields = document.getElementsByClassName("visibility-btn-icon");
    var type = "text";
    var add = "fa-eye-slash";
    var remove = "fa-eye";

    if (visibility)
    {
        type = "password";
        add = "fa-eye";
        remove = "fa-eye-slash";
    }

    for(var i = 0; i < aes_key_fields.length; i++)
    {
        aes_key_fields[i].type = type;
    }

    for(var i = 0; i < visibility_fields.length; i++)
    {
        visibility_fields[i].classList.remove(remove);
        visibility_fields[i].classList.add(add);
    }

    visibility = !visibility;
}

// #################################################################################
// Copy
// #################################################################################

var element = document.getElementsByClassName("copy-btn");
for(var i = 0; i < element.length; i++)
{
    element[i].addEventListener("click", copy);
}

function copy(event)
{
    var copyTextarea = document.querySelector('#aes-key-hidden');
    copyTextarea.select();
    document.execCommand('copy');

    var element = document.getElementsByClassName("aes-key");
    var previous_value = document.getElementById("aes-key-hidden").value;
    for(var i = 0; i < element.length; i++)
    {
        element[i].type = 'text';
        element[i].value = 'copied';
        element[i].classList.add('copied');
    }

    setTimeout(function()
    {
        var element = document.getElementsByClassName("aes-key");
        for(var i = 0; i < element.length; i++)
        {
            if (visibility)
            {
                element[i].type = 'text';
            }
            else
            {
                element[i].type = 'password';
            }

            element[i].value = previous_value;
            element[i].classList.remove('copied');
        }
    }, 1000);
}

// #################################################################################
// Derive key
// #################################################################################

document.getElementById("derive-key").addEventListener("click", deriveAKey);

async function deriveAKey()
{
    document.getElementById("aes-key-hidden").value = '';
    var element = document.getElementsByClassName("aes-key");
    for(var i = 0; i < element.length; i++)
    {
        element[i].classList.add('aes-key-animation');
        element[i].value = '';
    }

    let user = document.getElementById("user").value;
    let url = document.getElementById("url").value;
    let password = document.getElementById("password").value;

    try {
        var hexKey = await deriveKey(user, url, password);
    }
    catch(err) {
        var hexKey = "";
        displayError("Key derivation failed: " + err.message);
    }

    document.getElementById("aes-key-hidden").value = hexKey;
    var element = document.getElementsByClassName("aes-key");
    for(var i = 0; i < element.length; i++)
    {
        element[i].classList.remove('aes-key-animation');
        element[i].value = hexKey;
    }
}
});
