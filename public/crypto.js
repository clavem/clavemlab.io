// Password Based Key Derivation with Web Cryptography API
//
// Copyright (c) 2015 Info Tech, Inc.
// Provided under the MIT license.
// See LICENSE file for details.

if (typeof window === 'undefined')
{
    var WebCrypto = require('node-webcrypto-ossl');
    var crypto = new WebCrypto();
    var { TextEncoder } = require('text-encoding');
    var Promise = require('promise');

    module.exports = {
        deriveKey: deriveKey
    }

    var encodeBuffer = function(str, encoding) {
        return new Buffer(str, encoding);
    }
}
else
{
    var crypto = window.crypto;

    var encodeBuffer = function(str, encoding) {
        return new TextEncoder(encoding).encode(str);
    }
}

async function deriveKey(user, url, password)
{
    var iterations = 1000000;
    var hash = "SHA-512";

    let user_hash = await sha512(user);
    let url_hash = await sha512(url);
    let salt = await sha512(user_hash + url_hash);

    let baseKey = await crypto.subtle.importKey(
        "raw",
        stringToArrayBuffer(password),
        {"name": "PBKDF2"},
        false,
        ["deriveKey"]
    );

    let aesKey = await crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            "salt": stringToArrayBuffer(salt),
            "iterations": iterations,
            "hash": hash
        },
        baseKey,
        {"name": "AES-CBC", "length": 128},
        true,
        ["encrypt", "decrypt"]
    );

    let keyBytes = await crypto.subtle.exportKey("raw", aesKey);
    let key = await encode_ascii85(keyBytes);

    return key;
}


function stringToArrayBuffer(string)
{
    var encoder = new TextEncoder("utf-8");
    return encoder.encode(string);
}

function arrayBufferToHexString(arrayBuffer)
{
    var byteArray = new Uint8Array(arrayBuffer);
    var hexString = "";
    var nextHexByte;

    for (var i=0; i<byteArray.byteLength; i++)
    {
        nextHexByte = byteArray[i].toString(16);  // Integer to base 16
        if (nextHexByte.length < 2)
        {
            nextHexByte = "0" + nextHexByte;     // Otherwise 10 becomes just a instead of 0a
        }
        hexString += nextHexByte;
    }
    return hexString;
}

function arrayBufferToBase64(buffer)
{
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++)
    {
        binary += String.fromCharCode( bytes[ i ] );
    }
    return window.btoa( binary );
}

function encode_ascii85(buffer)
{
    var arr = new Uint32Array( buffer );

    var _chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-:+=^!/*?&<>()[]{}@%$#";

    if (!arr)
    {
        return null;
    }

    var out = "", c = _chars;

    for (var i = 0; i < arr.length; ++i)
    {
        var word = arr[i];
        var value = 0;
        for (var j = 0; j < 4; ++j)
        {
            var byteChunk = (word >>> 8*(4 - j - 1)) & 0xFF;
            value = value*256 + byteChunk;
        }
        var divisor = 85*85*85*85;
        while (divisor)
        {
            out += c.charAt(Math.floor(value/divisor) % 85);
            divisor = Math.floor(divisor/85);
        }
    }

    var encodedSize = arr.length*5;
    if (out.length !== encodedSize)
    {
        console.error("Bad Z85 conversion!");
    }

    return out;
}

function hex(buffer)
{
    var hexCodes = [];
    var view = new DataView(buffer);
    for (var i = 0; i < view.byteLength; i += 4)
    {
        // Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
        var value = view.getUint32(i)
        // toString(16) will give the hex representation of the number without padding
        var stringValue = value.toString(16)
        // We use concatenation and slice for padding
        var padding = '00000000'
        var paddedValue = (padding + stringValue).slice(-padding.length)
        hexCodes.push(paddedValue);
    }

    // Join all the hex strings into one
    return hexCodes.join("");
}

function sha512(str)
{
    // We transform the string into an arraybuffer.
    var buffer = encodeBuffer(str, "utf-8");
    return crypto.subtle.digest("SHA-512", buffer).then(function(hash)
    {
        return hex(hash);
    });
}
