function getHostName(url) {
    var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
    if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
        return match[2];
    }
    else {
        return null;
    }
}

function setUrl(url) {
    document.getElementById("url").value = getHostName(url);
}

function onTabQuery(tabs) {
    setUrl(tabs[0].url);
}

function onUpdated(tabId, changeInfo, tabInfo){
    setUrl(tabInfo.url)
}

function onActivated(tabId, windowId)
{
    chrome.tabs.query({currentWindow: true, active: true}, onTabQuery);
}

chrome.tabs.onActivated.addListener(onActivated);
chrome.tabs.onUpdated.addListener(onUpdated);
chrome.tabs.query({currentWindow: true, active: true}, onTabQuery);

document.addEventListener("DOMContentLoaded", function()
{
    "use strict";

    function setUsername(info) {
        document.getElementById("user").value = info[Object.keys(info)[0]] || "";
    }

    chrome.storage.local.get('username', setUsername);

    var error = document.getElementById("error");
    error.addEventListener("click", dismissError);

    displayError = function displayError(msg) {
        var error = document.getElementById("error");
        var error_msg = document.getElementById("error_msg");
        error_msg.textContent = msg;
        error.classList.add('visible');
        console.error(msg);
    }

    function dismissError() {
        var error = document.getElementById("error");
        error.classList.remove('visible');
    }

    document.getElementById("derive-key").addEventListener("click", function() {
        var user = document.getElementById("user").value || "";
        chrome.storage.local.set({'username': user});
    });
});
