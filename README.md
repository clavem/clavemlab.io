# Clavem

Unlike most other password management applications, Clavem doesn't store anything.
There is no place where your main password or your generated passwords are saved.
Instead, the exact same password is generated every time you enter the same inputs.

Clavem use SHA-512 as cryptographic hash and 1,000,000 PBKDF2 iterations as derivation algorithm.
This is made to ensure that it's very costly (or almost impossible) to guess your original password from a derived one. 

Your username and the website are hashed, then merged, then hashed, to create a salt unique for every user/website pair.

Clavem is made to allow you to provide a different password to each service you use, without the burden of having many different passwords to remember.

When you provide a password to a website, nothing but ethic prevents its administrators from stealing it and so having access to all of your accounts if your password is shared among other websites.
Even if administrators are nice enough to not use your password against your will, it can be stored in plain text and stole by hackers.
And even if your password is securely stored by the website, we believe their hash method is not as secure as the one provided by Clavem since it's probably too costly for any webservice. 

We think that if your memory is not extraordinary, you don't have much alternatives :

- remember one strong password and use it everywhere
- remember many weak password and use  them at different places 
- write down all passwords and travel with your physical password record
- remember one strong password and derive unique passwords mentally (cheaply) for every website (by adding the website name as a suffix to your strong password for example) 
- trust a third party service to store your passwords

Clavem is better since you just remember one (hopefully) strong password, shared with no one, and securely derive a new password for each of your accounts. And still nothing is stored by Clavem !

It doesn't prevent you from following common security advices such as :

- create a secure (long and random) password (use [diceware](http://www.diceware.com) for example)
- change your password as often as possible 
- keep your passwords secret (don't pin it on a wall) 

Don't trust us ! Fork this project on [gitlab.com](https://gitlab.com/clavem/clavem.gitlab.io) and host your own Clavem.