#!/usr/bin/env node

'use strict';

const os = require('os');
const clavem = require('./public/crypto');
const prompt = require('prompt');
const clipboardy = require('clipboardy');

const user = os.userInfo().username;
const url = process.argv[2];

prompt.message = '';
prompt.delimiter = ':';

var schema = {
    properties: {
        password: {
            hidden: true
        }
    }
};

prompt.start();

prompt.get(schema, function (err, result)
{
    var password = result.password;
    clavem.deriveKey(user, url, password).then(function(key) {
        clipboardy.writeSync(key);
    });
});
